from django.apps import AppConfig


class AnonymousUsersConfig(AppConfig):
    name = 'anonymous_users'
